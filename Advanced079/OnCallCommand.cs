﻿using System.Linq;
using Advanced079;
using Smod2.EventHandlers;
using Smod2.Events;

namespace Handlers
{
    internal class OnCallCommand : IEventHandlerCallCommand, IEventHandlerSetRole
    {
        private Main main;

        public OnCallCommand(Main main)
        {
            this.main = main;
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            if (main.GetConfigBool("a079_enabled"))
            {
                if(ev.Role == Smod2.API.Role.SCP_079)
                {
                    ev.Player.PersonalBroadcast(20, "<color=red><b>Update v[null]</b></color>\nDe nouvelles fonctions ont été implémentés.\nAppuyez sur % ou ~ pour plus d'info.", true);
                    ev.Player.SendConsoleMessage("<color=#ffff00>Note d'update\n</color>\n<color=#fff>Listes des nouvelles fonctionnalités\n- Fausse annonce de respawn FIM : .a079 fakefim\n- Fausse annonce de respawn CI : .a079 fakeci\n- Fausse annonce de mort SCP  : .a079 fakescp\n- Fermez toutes les portes non sécurisées : .a079 closed\n- Eteindre toutes les lumières : .a079 blackout\n- Récupérer les informations des générateurs : .a079 getgenerators\n- Récupérer les informations des portes : .a079 getdoors</color>\n<color=#707070>Plugin créer par Flo - Fan.</color>");
                }
            }
        }

        void IEventHandlerCallCommand.OnCallCommand(PlayerCallCommandEvent ev)
        {
            if (main.GetConfigBool("a079_enabled"))
            {
                string[] args = ev.Command.Split(' ');
                int level = ev.Player.Scp079Data.Level + 1;
                if (args[0] == "a079")
                {
                    if(ev.Player.TeamRole.Role == Smod2.API.Role.SCP_079)
                    {
                        if (args[1].ToLower() == "fakefim")
                        {
                            if(ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_fakemtf_mana") && level >= main.GetConfigFloat("a079_fakemtf_level"))
                            {
                                Capacities.FakeNTFAnnouncement();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_fakemtf_mana");
                                ev.ReturnMessage = "<color=green>Succès !</color>";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_fakemtf_mana")} | Level min : {main.GetConfigFloat("a079_fakemtf_level")})";
                                return;
                            }
                        }
                        else if(args[1].ToLower() == "fakeci")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_fakeci_mana") && level >= main.GetConfigFloat("a079_fakeci_level"))
                            {
                                Capacities.FakeCIAnnouncement();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_fakeci_mana");
                                ev.ReturnMessage = "<color=green>Succès !</color>";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_fakeci_mana")} | Level min : {main.GetConfigFloat("a079_fakeci_level")})";
                                return;
                            }
                        }
                        else if (args[1].ToLower() == "fakescp")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_fakescp_mana") && level >= main.GetConfigFloat("a079_fakescp_level"))
                            {
                                Capacities.FakeSCPDeadAnnouncement();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_fakescp_mana");
                                ev.ReturnMessage = "<color=green>Succès !</color>";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_fakescp_mana")} | Level min : {main.GetConfigFloat("a079_fakescp_level")})";
                                return;
                            }
                        }
                        else if (args[1].ToLower() == "closed")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_closealldoor_mana") && level >= main.GetConfigFloat("a079_closealldoor_level"))
                            {
                                Capacities.CloseAllDoors();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_closealldoor_mana");
                                ev.ReturnMessage = "<color=green>Succès !</color>";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_closealldoor_mana")} | Level min : {main.GetConfigFloat("a079_closealldoor_level")})";
                                return;
                            }
                        }
                        else if (args[1].ToLower() == "blackout")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_totalblackout_mana") && level >= main.GetConfigFloat("a079_totalblackout_level"))
                            {
                                Capacities.TotalBlackout();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_totalblackout_mana");
                                ev.ReturnMessage = "<color=green>Succès !</color>";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_totalblackout_mana")} | Level min : {main.GetConfigFloat("a079_totalblackout_level")})";
                                return;
                            }
                        }
                        else if (args[1].ToLower() == "getgenerators")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_getgenerators_mana") && level >= main.GetConfigFloat("a079_getgenerators_level"))
                            {
                                string[] generators = Capacities.GetGeneratorsStatus();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_getgenerators_mana");
                                ev.ReturnMessage = "<color=#fff>Liste des générateurs</color>\n" + string.Join("\n - ", generators);
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_getgenerators_mana")} | Level min : {main.GetConfigFloat("a079_getgenerators_level")})";
                                return;
                            }
                        }
                        else if (args[1].ToLower() == "getdoors")
                        {
                            if (ev.Player.Scp079Data.AP >= main.GetConfigFloat("a079_getdoors_mana") && level >= main.GetConfigFloat("a079_getdoors_level"))
                            {
                                string[] doors = Capacities.GetDoorsStatus();
                                ev.Player.Scp079Data.AP -= main.GetConfigFloat("a079_getdoors_mana");
                                ev.Player.SendConsoleMessage("<color=#fff>Liste des portes</color>");
                                foreach (string s in doors)
                                {
                                    ev.Player.SendConsoleMessage(s);
                                }

                                ev.ReturnMessage = "";
                                return;
                            }
                            else
                            {
                                ev.ReturnMessage = $"ERROR :\nEnergie et puissance manquante (Mana min : {main.GetConfigFloat("a079_getdoors_mana")} | Level min : {main.GetConfigFloat("a079_getdoors_level")})";
                                return;
                            }
                        }
                        else
                        {
                            ev.Player.SendConsoleMessage("<color=#fff>Listes des fonctionnalités\n- Fausse annonce de respawn FIM : .a079 fakefim\n- Fausse annonce de respawn CI : .a079 fakeci\n- Fausse annonce de mort SCP  : .a079 fakescp\n- Fermez toutes les portes non sécurisées : .a079 closed\n- Eteindre toutes les lumières : .a079 blackout\n- Récupérer les informations des générateurs : .a079 getgenerators\n- Récupérer les informations des portes : .a079 getdoors</color>\n<color=#707070>Plugin créer par Flo - Fan.</color>");
                        }
                    }
                    else
                    {
                        ev.ReturnMessage = "ACCESS DENIED.";
                    }
                }
            }
        }
    }
}